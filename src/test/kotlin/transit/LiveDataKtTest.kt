package transit

import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File
import java.time.LocalDate

internal class LiveDataKtTest {
    val lines = objectMapper.readValue<Lines>(File("data/lines.json"))
            .lines.associateBy { it.name }

    @Test
    internal fun parseDate() {
        assertThat(LocalDate.parse("12.03.18", LiveDataConverter.dateFormatter))
                .isEqualTo("2018-03-12")

    }

    @Test
    internal fun makeData() {


        val testFile = File("data/samples/s7-with-delay.json")

        val liveDataConverter = LiveDataConverter(lines)

        val allPositions = ArrayList<LiveDataAggrEntry>()
        val rawData = objectMapper.readValue<LiveDataRawWrapper>(testFile)
    }
}