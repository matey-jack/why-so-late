package transit

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Percentage.withPercentage
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.math.roundToInt
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GtfsTest {
    // https://blog.philipphauer.de/best-practices-unit-testing-kotlin/

    @Test
    internal fun testShortDistance() {
        val bhf_Friedrichstr = GeoLoc(52.519864, 13.387606)
        val s_Potsdamer_Platz = GeoLoc(52.509419, 13.376764)
        val actual = bhf_Friedrichstr.rectDist(s_Potsdamer_Platz).roundToInt()
        val good_enough = 1386  // measured in Google Maps: 1380
        assertEquals(good_enough, actual)
    }

    @Test
    internal fun testLongDistance() {
        val bhf_Friedrichstr = GeoLoc(52.519864, 13.387606)
        val s_Straußberg_Nord = GeoLoc(52.590977, 13.908826)
        val actual = bhf_Friedrichstr.rectDist(s_Straußberg_Nord).roundToInt()
        // we don't have such long distances in our application, therefore the error is ok
        val good_enough = 38989 // measured in Google Maps: 36130
        assertEquals(good_enough, actual)
    }

    @Test
    internal fun testDistanceOnMeridian() {
        val weltzeituhr_Alexanderplatz = GeoLoc(52.521302, 13.413668)
        val s_Blankenfelde = GeoLoc(52.336829, 13.415475)
        val actual = weltzeituhr_Alexanderplatz.rectDist(s_Blankenfelde).roundToInt()
        // as expected, error is very low even for the larger distance
        val good_enough = 20513  // measured in Google Maps: 20470
        assertEquals(good_enough, actual)
    }

    @Test
    internal fun locate_on_straight_Route() {
        val s1_nord = Route("s1n", "S1 Nord", listOf(
                Station("0", "Oranienburg", GeoLoc(52.754283, 13.248475)),
                Station("1", "Lehnitz", GeoLoc(52.741157, 13.263561)),
                Station("2", "Borgsdorf", GeoLoc(52.714559, 13.276889)),
                Station("3", "Birkenwerder", GeoLoc(52.687656, 13.288790)),
                Station("3", "Hohen-Neuendorf", GeoLoc(52.669682, 13.287024))
        ))
        val zug_unter_Autobahn = GeoLoc(52.699130, 13.284834)

        val actual = s1_nord.locateOnRoute(zug_unter_Autobahn)
        assertThat(actual.closestStation.name).isEqualTo("Birkenwerder")
        assertThat(actual.neighboringStation.name).isEqualTo("Borgsdorf")
        assertThat(actual.offRoute).isLessThan(0.1)
        assertThat(actual.relativePosition).isCloseTo(2.55, withPercentage(10.0))
    }

    @Test
    internal fun locate_on_Ringbahn() {
        val s42_ost = Route("s42o", "S42 Ost", listOf(
                Station("060058100532", "Südkreuz Bhf", GeoLoc(52.475468, 13.365579)),
                Station("060068201512", "U Tempelhof", GeoLoc(52.470694, 13.385754)),
                Station("060079221472", "U Hermannstr.", GeoLoc(52.467181, 13.431704)),
                Station("060078201462", "U Neukölln", GeoLoc(52.469282, 13.443692)),
                Station("060077106402", "Sonnenallee", GeoLoc(52.473822, 13.455998)),
                Station("060190001572", "Treptower Park", GeoLoc(52.493022, 13.460887)),
                Station("060120901552", "Ostkreuz Bhf", GeoLoc(52.502851, 13.469093)),
                Station("060120001542", "U Frankfurter Allee", GeoLoc(52.513613, 13.4753)),
                Station("060110012542", "Storkower Str.", GeoLoc(52.524195, 13.464884)),
                Station("060110004532", "Landsberger Allee", GeoLoc(52.528772, 13.455944)),
                Station("060110003512", "Greifswalder Str.", GeoLoc(52.540724, 13.438356)),
                Station("060110002782", "Prenzlauer Allee", GeoLoc(52.544802, 13.427419)),
                Station("060110001772", "U Schönhauser Allee", GeoLoc(52.549336, 13.415138)),
                Station("060007102722", "U Gesundbrunnen Bhf", GeoLoc(52.548637, 13.388372))
        ))
        val thaerbrücke = GeoLoc(52.525762, 13.458518)

        val actual = s42_ost.locateOnRoute(thaerbrücke)
        assertThat(actual.closestStation.name).isEqualTo("Landsberger Allee")
        assertThat(actual.neighboringStation.name).isEqualTo("Storkower Str.")
        assertThat(actual.offRoute).isLessThan(0.1)
        assertThat(actual.relativePosition).isCloseTo(8.3, withPercentage(10.0))
    }

    @Test
    internal fun projectSimple() {
        val p1 = GeoLoc(1.0, 2.0)
        val p2 = GeoLoc(1.0, 5.0)
        val p3 = GeoLoc(10.0, 3.0)
        val p4 = GeoLoc(10.0, 4.0)
        val line = Line(p1, p2)
        assertThat(line.project(p3)).isCloseTo(0.33, withPercentage(5.0))
        assertThat(line.project(p4)).isCloseTo(0.66, withPercentage(5.0))
    }

    @Test
    internal fun projectCenter() {
        val a = GeoLoc(0.0, 0.0)
        val b = GeoLoc(4.0, 4.0)
        val c = GeoLoc(2.0, 4.0)
        val d = GeoLoc(3.0, 0.0)
        val e = GeoLoc(3.0, 3.0)
        val line = Line(a, b)
        assertThat(line.project(c)).isCloseTo(0.75, withPercentage(5.0))
        assertThat(line.project(d)).isCloseTo(0.375, withPercentage(5.0))
        assertThat(line.project(e)).isCloseTo(0.75, withPercentage(5.0))
    }

    @Test
    internal fun projectOrthogonal() {
        val a = GeoLoc(2.0, 1.0)
        val b = GeoLoc(6.0, 3.0)
        val c = GeoLoc(0.0, 5.0)
        val d = GeoLoc(2.0, 6.0)
        val line = Line(a, b)
        assertThat(line.project(c)).isCloseTo(0.0, withPercentage(5.0))
        assertThat(line.project(d)).isCloseTo(0.5, withPercentage(5.0))
    }

    @Test
    internal fun projectInteresting() {
        val a = GeoLoc(1.0, 1.0)
        val b = GeoLoc(4.0, 3.0)
        val c = GeoLoc(2.0, 4.0)
        val d = GeoLoc(3.0, 0.0)
        val e = GeoLoc(3.0, 3.0)
        val line = Line(a, b)
        assertThat(line.project(c)).isCloseTo(9.0/13, withPercentage(5.0))
        assertThat(line.project(d)).isCloseTo(4.0/13, withPercentage(5.0))
        assertThat(line.project(e)).isCloseTo(10.0/13, withPercentage(5.0))
    }
}