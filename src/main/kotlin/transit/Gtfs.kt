package transit

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import java.io.File
import java.io.PrintWriter

const val EARTH_RADIUS = 6371e3 // in meters

/**
 * [lat] and [lon] are in degrees just like we use them on Google Maps, GTFS & the like.
 */
data class GeoLoc(val lat: Double, val lon: Double) {
    /**
     * Distance of points in meters
     *
     * We use equi-rectangular approximation, which will yield good results,
     * because distances in our case are so small. We still need the cos(...) factor
     * because lon-degrees are much less in meters as far from the equator as we are
     * living. Multiplying with EARTH_RADIUS isn't necessary as long as we only compare
     * distances to find closest points. But it makes for much more useful debugging
     * output.
     * Source of formula: https://www.movable-type.co.uk/scripts/latlong.html
     */
    fun rectDist(other: GeoLoc): Double {
        val x = (lon - other.lon) * Math.cos((lat + other.lat) / 2)
        val y = (lat - other.lat)
        return Math.toRadians(Math.sqrt(x * x + y * y)) * EARTH_RADIUS
    }
}


class Line(val p1: GeoLoc, p2: GeoLoc) {
    // We don't call this class GeoLine, because it works on a flat-earth model.
    // this is okay as long as we only use it for neighboring stations where distances are
    // usually <1 km and at most <3km.

    // don't pollute main GeoLoc with flat-world functions
    fun GeoLoc.pointTo(p: GeoLoc): GeoLoc = GeoLoc(p.lat - lat, p.lon - lon)

    fun GeoLoc.dotProd(p: GeoLoc): Double = p.lat * lat + p.lon * lon

    val dp = p1.pointTo(p2)
    val dpScal = dp.dotProd(dp)

    fun project(p: GeoLoc): Double = dp.dotProd(p1.pointTo(p)) / dpScal
}

data class Station(val id: String, val name: String, val loc: GeoLoc) {
    val shortName
        @JsonIgnore
        get() = name.drop(2).replace(" (Berlin)", "")

    val hasSBahn
        @JsonIgnore
        get() = name.startsWith("S ") || name.startsWith("S+U")
}

/**
 * Result of locating [loc] on [route].
 * This stores more information that necessary so we can debug better and also to use different
 * tolerances for deciding
 */
data class RelativePosition(
        val loc: GeoLoc,
        val route: Route,
        val closestIndex: Int,
        val closestStation: Station
) {
    // all of those are useful properties part of the public interface
    // we currently keep them as fields, but might switch some to become getters to save some memory.
    val distanceToStation = closestStation.loc.rectDist(loc)

    val isOnStation = isOnStation()
    fun isOnStation(tolerance: Int = 300) = distanceToStation < tolerance

    val isBefore: Boolean = when {
        (closestIndex == 0) -> false
        (closestIndex + 1 == route.longestTrip.size) -> true
        else -> {
            val s1 = route.longestTrip[closestIndex - 1]
            val s2 = route.longestTrip[closestIndex + 1]
            val line = Line(s1.loc, s2.loc)
            line.project(loc) < line.project(closestStation.loc)
        }
    }

    val neighboringIndex = if (isBefore) closestIndex - 1 else closestIndex + 1
    val neighboringStation = route.longestTrip[neighboringIndex]
    val distanceToNeighbor = neighboringStation.loc.rectDist(loc)

    /**
     * Sort-of percentage indicator how much [loc] is veering off the line between [closestStation]
     * and [neighboringStation]. A user should decide how much they can tolerate.
     */
    val offRoute = (distanceToStation + distanceToNeighbor) /
            closestStation.loc.rectDist(neighboringStation.loc) -
            1

    /**
     * Index of the first station plus the sort-of percentage that [loc] is closer to
     * the second station. This number should increase or decrease more or less steadily
     * as a vehicle moves forward or backward on the [route].
     */
    val relativePosition = (distanceToStation / (distanceToStation + distanceToNeighbor)).let { relativeDistance ->
        if (closestIndex < neighboringIndex) {
            closestIndex + relativeDistance
        } else {
            closestIndex - relativeDistance
        }
    }

    val preposition = if (isOnStation) "=" else if (isBefore) "<" else ">"
    val prettyString: String
        get() {
            val pos = "%6.3f".format(relativePosition)
            return "$pos  $preposition ${closestStation.shortName}"
        }
}


data class Route(val id: String, val name: String, var longestTrip: List<Station> = listOf()) {
    val stationString
        @JsonIgnore
        get() = longestTrip.map(Station::shortName).joinToString()

    val isSBahn: Boolean
        @JsonIgnore
        get() {
            val replacementBusSuffix = "_700"
            return name.startsWith("S") && name.length > 1 && name[1].isDigit() &&
                    !id.endsWith(replacementBusSuffix)
        }

    override fun toString() = "$name ($id): $stationString"

    fun longerOne(other: Route) =
            if (this.longestTrip.size > other.longestTrip.size) this else other

    fun locateOnRoute(loc: GeoLoc): RelativePosition {
        val (i, s) = longestTrip.withIndex().minBy { it.value.loc.rectDist(loc) }!!
        return RelativePosition(loc, this, i, s)
    }
}

// for JSON serialization
// best practice to have a JSON object at top-level, not a list, to make deserialization easy.
data class Lines(val lines: List<Route>)

class Gtfs {
    // id -> full object (which includes id)
    val sBahnRoutes = HashMap<String, Route>()

    // id -> full object (which includes id)
    val sBahnStations = HashMap<String, Station>()

    // trip id -> route object (which includes route id)
    val sBahnTripIds = HashMap<String, Route>()

    /* all those [val]s are cheating, because we mutate the collections.
    * OTOH they are immutable after initialization.
    * maybe refactor code to make this clear.
    * */

    // only really skips header record when we also set some [header]
    val csvFormat = CSVFormat.DEFAULT.withSkipHeaderRecord().withHeader("")

    fun readRouteIds() {
        val csvData = File("data/static/routes.txt")
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val route = Route(csvRecord[0], csvRecord[2])
                if (route.isSBahn) {
                    sBahnRoutes[route.id] = route
                }
            }
        }
    }

    fun readStationIds() {
        val csvData = File("data/static/stops.txt")
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser.records) {
                if (csvRecord[4].isBlank() || csvRecord[5].isBlank()) {
                    println("Not adding station ${csvRecord[2]}, because geo location is missing.")
                    continue
                }
                val loc = GeoLoc(csvRecord[4].toDouble(), csvRecord[5].toDouble())
                val s = Station(csvRecord[0], csvRecord[2], loc)
                if (s.hasSBahn) {
                    sBahnStations[s.id] = s
                }
            }
        }
    }

    fun readTripIds() {
        val csvData = File("data/static/trips.txt")
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val routeId = csvRecord[0]
                val tripId = csvRecord[2]
                // ignore routes which we haven't saved
                sBahnRoutes[routeId]?.let { sBahnTripIds[tripId] = it }
            }
        }
    }

    /**
     * Add Station lists to [longestTrips] of Routes in [sBahnRoutes].
     */
    fun readTripStations() {
        // assume that the file is grouped by tripId
        // (we'll check this assumption on the way)
        val csvData = File("data/static/stop_times.txt")
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            var currentTrip: String? = null
            var currentStations: MutableList<Station> = ArrayList()
            val finishedTrips = HashSet<String>()
            for (csvRecord in parser) {
                val tripId = csvRecord[0]
                if (currentTrip == null) {
                    currentTrip = tripId
                } else if (currentTrip != tripId) {
                    assert(tripId !in finishedTrips) { "tripId occurring twice, not grouped together! " }

                    addStationsToRoute(currentTrip, currentStations)
                    finishedTrips.add(currentTrip)
                    currentTrip = tripId
                    currentStations = ArrayList()
                }
                val station = sBahnStations[csvRecord[3]]
                if (station != null) {
                    currentStations.add(station)
                }
            }
        }
    }

    fun addStationsToRoute(trip: String, stations: List<Station>) {
        val route = sBahnTripIds[trip]
        if (route != null) {
            if (route.longestTrip.size < stations.size) {
                route.longestTrip = stations
            }
        }
    }


    fun loadData() {
        readRouteIds()
        readStationIds()
        readTripIds()
        readTripStations()
    }

    fun printRoutes() {
        println("The following ${sBahnRoutes.size} routes have been found: ")
        sBahnRoutes.values.sortedBy { it.name }.forEach { println(it) }
    }

    // name -> Route object
    // by our own definition a line is just the longest [Route]
    // among all Routes with the same name.
    val sBahnLines
        get() = sBahnRoutes.values
                .groupingBy { it.name }
                .reduce { _, r1, r2 -> r1.longerOne(r2) }

    fun printLine(route: Route) {
        for (s in route.longestTrip) {
            println("""Station("${s.id}", "${s.shortName}", GeoLoc(${s.loc.lat}, ${s.loc.lon})),""")
        }
    }

    fun getBoundingBox() {
        readStationIds()
        val eastern = sBahnStations.values.maxBy { it.loc.lon }!!
        val western = sBahnStations.values.minBy { it.loc.lon }!!
        val northern = sBahnStations.values.maxBy { it.loc.lat }!!
        val southern = sBahnStations.values.minBy { it.loc.lat }!!
        println("East: ${eastern.shortName}")
        println("West: ${western.shortName}")
        println("North: ${northern.shortName}")
        println("South: ${southern.shortName}")
        println("Rectangle: look_minx=${southern.loc.lon}&look_maxx=${northern.loc.lon}" +
                "&look_miny=${western.loc.lat}&look_maxy=${eastern.loc.lat}")

        /*
Rectangle: look_minx=52296921&look_maxx=52754362&look_miny=13066848&look_maxy=13908894
East: Strausberg Nord
West: Potsdam Hauptbahnhof/Nord ILB
North: Oranienburg Bhf
South: Königs Wusterhausen Bhf
Note: KW liegt auf ähnlicher Höhe wie Rangsdorf und Ludwigsfelde (außerhalb A10)
Potsdam ähnlich wie Falkensee/Finkenkrug (alle innerhalb A10, aber Nauen liegt außerhalb)
Oranienburg (außerhalb A10) deutlich nördlich von Velten (innerhalb A10) und Bernau (außerhalb)
übrigens liegt ansonsten nur noch Straußberg außerhalb der A10, alle anderen Endhalte sind innerhalb.
Berlin-Buch ist übrigens der einzige Stadtteil außerhalb der A10!
Im Süden und Westen verläuft die Autobahn teilweise 10 km entfernt vom Stadtrand.
         */
    }
}

fun saveRoutes(fileName: String, routes: List<Route>) {
    val objectMapper = jacksonObjectMapper()
    objectMapper.writeValue(File(fileName), Lines(routes))
}

fun saveLinesTabular(fileName: String, routes: List<Route>) {
    File(fileName).printWriter().use { printWriter ->
        for (route in routes) {
            printWriter.println(route.name)
            printWriter.println("=".repeat(route.name.length))
            printWriter.println("")
            printLineTo(printWriter, route)
            printWriter.println("")
        }
    }
}

private fun printLineTo(printWriter: PrintWriter, route: Route) {
    if (route.longestTrip.isEmpty()) {
        printWriter.println("(no stations)")
        return
    }
    var distanceFromStart = 0.0
    var prevStation = route.longestTrip.first()
    for (station in route.longestTrip) {
        distanceFromStart += station.loc.rectDist(prevStation.loc)
        prevStation = station
        printWriter.print("%5.2f km    ".format(distanceFromStart / 1000))
        printWriter.println(station.shortName)
    }
}

fun main(args: Array<String>) {
    val gtfs = Gtfs()
    gtfs.getBoundingBox()
    gtfs.loadData()
    val lines = gtfs.sBahnLines
//    gtfs.printLine(lines["S42"]!!)
    saveRoutes("data/lines.json", lines.values.toList())
    saveLinesTabular("data/lines.txt", lines.values.toList())
}
