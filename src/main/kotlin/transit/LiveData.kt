package transit

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KLogging
import java.io.File
import java.text.DecimalFormat
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

internal val objectMapper = jacksonObjectMapper().apply {
    configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}

data class LiveDataRawWrapper(
        // time hh:mm:ss,    main data
        val ts: String, val t: List<LiveDataRawEntry>
)

data class LiveDataRawEntry(
        // short Name, Line head sign,  trainRunId,   date,
        val n: String, val l: String, val i: String, val rd: String,

        // geoloc lon    and   -lat,    Real-Time delay
        val x: String, val y: String, val rt: String?
)

data class LiveDataAggrEntry(
        val routeName: String,
        val runId: String,
        val dateTime: LocalDateTime,
        // train running in direction of increasing indexes on the line
        val forward: Boolean,
        val position: RelativePosition,
        val delay: Int,
        val count: Int
) {
    val key
        get() = AggregationKey(routeName, position.preposition, position.closestStation.shortName, forward)
}

data class AggregationKey(
        val routeName: String,
        val preposition: String,
        val closestStation: String,
        val forward: Boolean
)

data class AggregationValue(val delay: Int, val count: Int)
// data class AggregationValue(var delay: Int = 0, var count: Int = 1)

class LiveDataConverter(val lines: Map<String, Route>) {
    companion object : KLogging() {
        val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yy")
    }

    fun parseCoordinate(s: String): Double =
            s.toInt() / 1e6

    fun getGeoLoc(entry: LiveDataRawEntry) =
            GeoLoc(parseCoordinate(entry.y), parseCoordinate(entry.x))

    fun findStationIndex(stations: List<Station>, name: String) =
            stations.indexOfFirst { it.name == name }

    fun shortenRunId(id: String): String {
        val parts = id.split("/")
        assert(parts[0] == "84")
        assert(parts[2] == "18")
        assert(parts[4] == "86")
        return "${parts[1]}/${parts[3]}"
    }

    fun convertEntry(entry: LiveDataRawEntry, time: LocalTime): LiveDataAggrEntry? {
        val loc = getGeoLoc(entry)
        val routeName = entry.n.trim()
        val route = lines[routeName] ?: return null
        val position = route.locateOnRoute(loc)
        val destination = findStationIndex(route.longestTrip, entry.l)
        // special cases for first and last station which always determine direction.
        // only if destination is in the middle of line, consider actual train position.
        val isForward = (destination != 0) &&
                (destination == route.longestTrip.size - 1) ||
                ((destination == position.closestIndex) && position.isBefore) ||
                destination > position.closestIndex

        val date = java.time.LocalDate.parse(entry.rd, dateFormatter)
        val delay = entry.rt?.toInt() ?: 0
        return LiveDataAggrEntry(routeName, shortenRunId(entry.i), date.atTime(time),
                isForward, position, delay, 1)
    }
}

typealias AggregationMap = Map<AggregationKey, AggregationValue>

class Aggregator {
    companion object {
        val df = DecimalFormat(" 0.00")
    }

    fun aggregateOneLine(allPositions: List<LiveDataAggrEntry>, route: Route) {
        val results = allPositions.filter { it.routeName == route.name }
                .groupBy { it.key }
                .mapValues { (_, entries) -> sumEntries(entries) }

        val namePadding = route.longestTrip.map { it.shortName.length }.max()!!

        for (station in route.longestTrip.indices) {
            val stationName = route.longestTrip[station].shortName
            for (preposition in listOf("<", "=", ">")) {
                val fwdCounts = formatResult(results[AggregationKey(route.name, preposition, stationName, true)])
                val retCounts = formatResult(results[AggregationKey(route.name, preposition, stationName, false)])
                println("$preposition ${stationName.padEnd(namePadding)}\t\t\t$fwdCounts\t$retCounts")
            }
        }
    }

    fun formatResult(aggregationValue: AggregationValue?): String {
        val value = aggregationValue ?: AggregationValue(0, 0)
        val delay = if (value.count == 0) " -.--" else df.format(value.delay.toDouble() / value.count)
        val count = value.count.toString().padStart(4)
        return "$delay ($count)"
    }

    fun sumEntries(entries: List<LiveDataAggrEntry>): AggregationValue {
        val delay = entries.map { it.delay }.sum()
        return AggregationValue(delay, entries.count())
    }
}

class TrainRun(val lines: Map<String, Route>) {
    fun trainRunsByDelay(allPositions: List<LiveDataAggrEntry>): List<Pair<String, Int>> {
        // allPositions.groupBy { it.runId }.mapValues { it.value.map { it.delay }.max() }
        // below expression does the same, but its result type knows that resulting map values are NonNullable!

        val maxDelayPerRunId = allPositions.groupingBy { it.runId }
                .fold(
                        { _, run -> run.delay },
                        { _, maxDelay, run -> maxOf(maxDelay, run.delay) }
                )

        return maxDelayPerRunId.toList().sortedByDescending { it.second }
    }

    fun printTrainRun(runPositions: List<LiveDataAggrEntry>, line: Route) {
        val stationPositions = runPositions.groupingBy { it.position.closestIndex }
                .reduce { _, a, b -> if (a.dateTime < b.dateTime) b else a }
                .values
                .sortedBy { it.dateTime }
        printPositions(stationPositions, line.name)
    }

    fun printRunDetailed(runPositions: List<LiveDataAggrEntry>, line: Route) {
        val sortedPositions = runPositions.sortedBy { it.dateTime }
        printPositions(sortedPositions, line.name)
    }

    fun printPositions(stationPositions: List<LiveDataAggrEntry>, lineName: String) {
        val firstPosition = stationPositions.first()
        println("$lineName - ${firstPosition.runId} started at ${firstPosition.dateTime}")
        for (position in stationPositions) {
            val delay = position.delay.toString().padStart(3)
            println("$delay – ${position.position.prettyString}")
        }
    }

    fun printMostDelayedTrainRun(allPositions: List<LiveDataAggrEntry>) {
        for (mostDelayedRunId in trainRunsByDelay(allPositions).take(10).map { it.first }) {
            // todo: check whether runIds can repeat on different days!
            val runPositions = allPositions.filter { it.runId == mostDelayedRunId }
            val line = lines[runPositions.first().routeName]!!
//            printTrainRun(runPositions, line)
            printRunDetailed(runPositions, line)
            println("-------")
        }
    }
}

fun main(args: Array<String>) {
    val lines = objectMapper.readValue<Lines>(File("data/lines.json")).lines.associateBy { it.name }

    val dynFiles = File("data/dyn").listFiles { _, name -> name.startsWith("2018-03-27T1") }

    val liveDataConverter = LiveDataConverter(lines)

    val allPositions = ArrayList<LiveDataAggrEntry>()
    for (file in dynFiles) {
        file.forEachLine { line ->
            val rawData = objectMapper.readValue<LiveDataRawWrapper>(line)
            val time = LocalTime.parse(rawData.ts)
            rawData.t.mapNotNullTo(allPositions) { liveDataConverter.convertEntry(it, time) }
        }
    }

    Aggregator().aggregateOneLine(allPositions, lines["S7"]!!)
//    TrainRun(lines).printMostDelayedTrainRun(allPositions)
}
