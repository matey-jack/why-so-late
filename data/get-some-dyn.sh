#!/bin/bash

cd data/dyn || cd dyn

while 
    true
do 
    echo $(date -Iseconds)
    fileName="$(date +'%FT%H').json"
    curl -s -X POST \
    'http://fahrinfo.vbb.de/bin/query.exe/dny?look_miny=52296921&look_maxy=52754362&look_minx=13066848&look_maxx=13908894&tpl=trains2json2&look_productclass=1&look_json=yes&performLocating=1&look_nv=zugposmode|2|' \
    -H 'Accept-Encoding: gzip, deflate' -H 'Content-Length: 0' --compressed \
    >> $fileName
    echo >> $fileName
    
    # LiveMap in browser polls every 30 secs, so we do that, too.
    sleep 30
done
