import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.gitlab.matey-jack"
version = "0.1-SNAPSHOT"

plugins {
    application
    kotlin("jvm") version "1.2.71"
    id("com.github.ben-manes.versions") version "0.20.0"
}

// property can't be used directly in plugins-section, duplicate until we find solution.
val kotlinVersion: String by extra("1.2.71")
// 5.1 boasting improved Kotlin support, yay!!
val junitJupiterVersion: String by extra("5.3.1")

buildscript {
    // since the new [plugins { }] DSL is still incubating, we use the old way to get the newest JUnit.
    // this concerns all entries from here until [apply { plugin(...) }]
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.2.0")
    }

}

apply {
    plugin("org.junit.platform.gradle.plugin")
}

application {
    mainClassName = "transit.LiveDataKt"
}

repositories {
    mavenCentral()
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
    // slf4j comes indirectly with kotlin-logging
    compile("io.github.microutils:kotlin-logging:1.4.9")
    compile("ch.qos.logback:logback-classic:1.2.3")

    compile("org.apache.commons:commons-csv:1.6")
    compile("com.fasterxml.jackson:jackson-base:2.9.7")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")

    testCompile("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
    testCompile("org.junit.jupiter:junit-jupiter-api:${junitJupiterVersion}")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:${junitJupiterVersion}")
    testCompile("org.assertj:assertj-core:3.9.0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

