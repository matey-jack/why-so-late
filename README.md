What is this good for?
======================

Berlin's S-Bahn has a few single-tracked sections and those lead to delays from
one train to propagate to the opposing train (same line, other direction). 

I want to know how much this happens and on which lines and which sections so I
take same live train data as published by VBB and aggregate and analyze that.

Currently we can show average delays per station and per line.

Since different types of delays propagate differently, one of the next things
to do would be to use histograms (which amount of delay occurs how often) to
get a better idea if the network should be optimized to for resiliancy against
small delays or big delays. (Small would be < 3 minutes, anything where the 
crossing point of a train and the opposing train would happen at the same section
of double track. Whereas in big delays, the crossing point changes to another
side of a single-track section.)

How does it work?
=================

We extract Line-data from VBB's GTFS feed, so we know which station is on which 
line. 

The live data consists of a list of train positions which are basically 
 - the line number
 - line direction
 - delay in minutes
 - and train position as GPS

For each train position we iterate through all the stations of the line to make
a guess at which station or between which stations the train is currently. 
Then we collect the average delay for each segment of the line.


Backlog
=======

Backend / aggregator
====================

DONE
- show average delays per station for one _line_ in a time interval such as a few hours or a few days
- find and show all times for one train id
  - find some trains that are very late and show their runs.
- current position now shown _between_ stations instead of _at_ station.
  - better matching of coordinates to stations

IN PROGRESS
- distinguish positions _at_ and _between_ stations
   - advantage: rest time at stations more transparent (now it's rather randomly counted as before or after)
   - advantage: we can see which stations have longer rest times and thus compensate delays
   - advantage: this information can be used to validate data (by finding non-plausible outputs)

READY

- show average delay per line at specific comparison stations (like Friedrichstraße, Ostkreuz) or at last station of each line. 
  - when calculating this, first groupBy train-run-id (getting max delay) and then by line


PLANS and IDEAS
- continuously fetch and store data in the cloud
  - one lambda to fetch and store in Kinesis
  - another lambda to batch up raw data to S3 (one file per 12 hours or so)
  - a third one can do the aggregation / digestion for the frontend


Frontend
========

Note that there isn't any nice result presentation yet. Currently lines and 
delays are just printed as text.

- Show timeline with aggregated delays, so that a timeframe can be selected and the line diagram can be displayed for that timeframe.
- line diagram with bars instead of textual numbers
- Show list of timelines for each line and let one line be selected
- Show timeline below the line diagram so that we can "zoom and scroll through time"
- Support "line bundles" such as "Stadtbahn (Lichtenberg/Rummelsburg – Westkreuz)", "Tunnelbahn (Priesterweg/Leberbrücke – Schönholz/Pankow)", and "Görlitzer Bahn (Adlershof – Treptower Park/Neukölln)".
- Support "line pairs" such as S8/S85, S9/S45, S7/S75. (Note: S25 pairs with S2 in South, but S1 in North; S8 with S2 in North and S46 in South; so don't take this too far...)

